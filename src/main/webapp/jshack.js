window.onload = function() {

  var url = window.location.origin + '/webapps/prot-Building%20block-BBLEARN/';
  /* var url = 'https://demo.prot-os.com/webapps/prot-Building%20block-BBLEARN/';*/
  /* var url = 'http://localhost:9876/webapps/prot-Building%20block-BBLEARN/';*/
  var timer;
  var json = [];
  var studentPosts = [];
  var state_url;

  var getUrlParam = function(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;
    for (i = 0; i < sURLVariables.length; i++) {
      sParameterName = sURLVariables[i].split('=');
      if (sParameterName[0] === sParam) {
        return sParameterName[1] === undefined ? true : sParameterName[1];
      }
    }
  }
  var courseid = getUrlParam('course_id');

  var applyLimit = function() {
    var fid = getUrlParam('forum_id');
    for (var i = 0; i < json.length; i++) {
      if (json[i].id === fid) {
        return json[i];
      }
    }
  }

  var applyPosts = function() {
    var fid = getUrlParam('forum_id');
    for (var i = 0; i < studentPosts.length; i++) {
      if (studentPosts[i].forumid === fid) {
        return studentPosts[i];
      }
    }
  }


  var setBtnsState = function(b) {
    var submit_top = document.getElementById('bottom_submitBtn');
    var submit_down = document.getElementById('top_submitBtn');
    if (!submit_top || !submit_down) {
      return;
    }
    submit_top.disabled = b;
    submit_down.disabled = b;

    if (b) {
      submit_top.style.display = 'none';
      submit_down.style.display = 'none';
      tinymce.activeEditor.getBody().setAttribute('contenteditable', false);
      return;
    }
    submit_top.style.display = 'inline';
    submit_down.style.display = 'inline';
    tinymce.activeEditor.getBody().setAttribute('contenteditable', true);
  }

  var getLocale = function() {
    var xhttp = new XMLHttpRequest();
    var forumid = getUrlParam('forum_id');

    xhttp.onreadystatechange = function() {
      if (xhttp.readyState == 4 && xhttp.status == 200) {
        /* console.log('this is the locale of user: ' + xhttp.responseText);*/
      }
    };
    xhttp.open("GET", url + "GetLocale", true);
    xhttp.send();
  }


  var makeGetRequest = function(nextReq, render) {
    var xhttp = new XMLHttpRequest();
    var forumid = getUrlParam('forum_id');

    xhttp.onreadystatechange = function() {
      if (xhttp.readyState == 4 && xhttp.status == 200) {
        if (xhttp.responseText === 'instructor') {
          clearInterval(timer);
          return;
        }
        json = JSON.parse(xhttp.responseText);
        if (nextReq) {
          nextReq(render);
        }
      }
    };

    xhttp.open("GET", url + "MainController?course_id=" + encodeURIComponent(courseid)  + "&op=getStudentLimits&foroid=" + forumid, true);
    xhttp.send();
  }

  var makeGetRequest1 = function(render) {
    var xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function() {
      if (xhttp.readyState == 4 && xhttp.status == 200) {
        studentPosts = JSON.parse(xhttp.responseText);
        if (render) {
          render();
        }
      }
    };
    xhttp.open("GET", url + "MainController?course_id=" + encodeURIComponent(courseid)  + "&op=studentPosts", true);
    xhttp.send();
  }

  var render = function() {
    var limits = applyLimit();
    if (!limits) {
      clearInterval(timer);
      return;
    }


    var replyBtn = document.getElementsByClassName('dbThreadMessage')[0];
    /* var threadBody = document.getElementsByClassName('dbThreadBody');*/
    var threadBtns = document.getElementsByClassName('threadButtons');
    var threadBody = document.getElementsByClassName('dbThreadBody');

    var threadArea = document.getElementById('threadArea');
    var testing = document.getElementById('testing');

    var charslimit = +limits.charslimit;
    var postslimit = +limits.postslimit;
    var posts = applyPosts();
    var postsmade = +posts.posts;
    var str = postslimit === 0 ? '<div id="postslimit" style="float:right; font-size: 17px; color:#058BAA; margin-right: 10px;">' +
                                 'Has escrito ' + postsmade  + 
                                 ' de <i> n </i> ilimitados  </div>' :
              '<div id="postslimit" style="float:right; font-size: 17px; color:#058BAA; margin-right: 10px;">' +
              'Has escrito ' + postsmade +
              ' de ' + 
              postslimit + ' disponibles' +
              ' </div>';


    makeGetRequest();
    makeGetRequest1();

    if (+postslimit !== 0 && +postsmade >= +postslimit) {

      str = '<div id="postslimit" style="float:right; font-size: 17px; color:#FF0000; margin-right: 10px;">' + 
            ' Has alcanzado el limite maximo de mensajes (' + postslimit + ') </div>';

      [].forEach.call(threadBody, function(row){
        row.innerHTML = '<a style="padding: 5px; color: #ffffff; cursor: pointer; background-color: gray" class="fakeBtn"> Responder </a>';
      });
      var fakeBtns = document.getElementsByClassName('fakeBtn');
      [].forEach.call(fakeBtns, function(row){
        row.onclick = function() {
          alert('Has excedido los mensajes posibles');
        }
      });

      [].forEach.call(threadBtns, function(btnRow){
        btnRow.style.display = 'none';
      });
      
      if (threadArea && !testing) {
        threadArea.innerHTML += '<div id="testing" style="margin-bottom:20px;">' + str + '</div>';
      }
      else if (testing) {
        testing.innerHTML = '<div id="testing">' + str + '</div>';
      }

      setBtnsState(true);
      /* return;*/
    }
    else {
      setBtnsState(false);
      [].forEach.call(threadBtns, function(btnRow){
        btnRow.style.display = 'inline-block';
      });

      [].forEach.call(threadBody, function(row){
        row.innerHTML = '';
      });

      if (threadArea && !testing) {
        threadArea.innerHTML += '<div id="testing" style="margin-bottom:20px;">' + str + '</div>';
      }
      else if (testing) {
        testing.innerHTML = '<div id="testing">' + str + '</div>';
      }
    }

    var nextPage = document.getElementById('stepcontent1');
    if (!nextPage) return;
    var posts = nextPage ? document.getElementById('stepcontent1').children[0].children[1].children[0] : null;
    str = postslimit === 0 ? '<div id="postslimit" style="float:right; font-size: 17px; color:#058BAA; margin-right: 10px;">' +
                             'Escribiendo ' + (postsmade + 1) + 
                             ' de <i> n </i> ilimitados  </div>' :
          '<div id="postslimit" style="float:right; font-size: 17px; color:#058BAA; margin-right: 10px;">' +
          'Escribiendo ' + (postsmade + 1) +
          ' de ' + 
          postslimit + ' disponibles' +
          ' </div>';

    if (+postslimit !== 0 && +postsmade >= +postslimit) {
      str = '<div id="postslimit" style="float:right; font-size: 17px; color:#FF0000; margin-right: 10px;">' + 
            ' Has alcanzado el limite maximo de mensajes (' + postslimit + ') </div>';
    }

    posts.innerHTML = str;
    
    var a = document.getElementsByClassName('mceStatusbar mceFirst mceLast')[0].children[2];

    var txt = tinyMCE.activeEditor.getContent({format : 'text'});
    if (+charslimit === 0) {
      a.innerHTML = 'sin limite';
      return;
    }
    if (+postsmade >= +postslimit) {
      a.innerHTML = '';
      return;
    }
    if (txt.length > charslimit) {
      tinyMCE.activeEditor.setContent(txt.substring(0, charslimit));
      return;
    }
    a.innerHTML = txt.length + '/' + charslimit + ' caracteres';
  }

  getLocale();
  makeGetRequest(makeGetRequest1, render);

  timer = setInterval(render, 500);
}

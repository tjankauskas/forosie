<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="/bbNG" prefix="bbNG"%>
<bbNG:learningSystemPage>
  <bbNG:pageHeader>
    <bbNG:pageTitleBar title="Configuración avanzada de Foros de Curso" showTitleBar="true"></bbNG:pageTitleBar>
    <div style="font-style: italic; margin-top: 8px; color: #393939">
      En la configuración avanzada de foros se puede establecer el límite de posts máximo y el máximo número de caracteres permitido para cada foro. Puede establecer los valores por defecto con los que se crearán los nuevos foros. También puede modificar los valores de cualquier foro, o aplicar los mismos valores a varios foros seleccionando los foros a cambiar y haciendo click en "Edición multiple". En cualquier caso, los valores quedarán guardados una vez establecidos de manera automática. Además puede buscar cualquier foro a través del campo de búsqueda habilitado para tal efecto.
    </div>
  </bbNG:pageHeader>
  <div id="app"> </div>
</bbNG:learningSystemPage>

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <link rel="stylesheet" type="text/css" href="forosIE.css">
  <script src="https://use.fontawesome.com/beb6bf58a7.js"></script>
  <script src="forosIE.js"></script>
</head>


package com.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import blackboard.platform.BbServiceManager;
import com.google.gson.Gson;


@SuppressWarnings("serial")
public class GetLocale extends HttpServlet {
    private class Translations {
        private String alert_message, algo;
        public Translations(String locale) {
            if (locale.equals("es_ES")) {
                alert_message = "No puedes escribir mas mensajes, has excedido el limite";
                algo = "blablabla en espanol";
            }
            else { // por defecto ingles
                alert_message = "You cannot write more posts, you have exceeded the limit";
                algo = "blablabla in english";                
            }
        }
    }
    
    public GetLocale() {
        super();
    }
    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException {
        response.getWriter().write(new Gson().toJson(new Translations(BbServiceManager.getLocaleManager().getLocale().getLocale())));
    }
}

package com.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import blackboard.db.BbDatabase;
import blackboard.db.ConnectionManager;
import blackboard.db.ConnectionNotAvailableException;
import blackboard.platform.context.ContextManagerFactory;
import blackboard.persist.PersistenceException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import com.google.gson.Gson;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.LinkedList;
import java.util.Calendar;
import java.util.Date;
import blackboard.persist.discussionboard.ForumDbLoader;
import blackboard.persist.Id;
import blackboard.persist.discussionboard.ConferenceDbLoader;
import blackboard.data.discussionboard.Message;
import blackboard.data.discussionboard.Conference;
import blackboard.data.discussionboard.Forum;
import blackboard.persist.discussionboard.MessageDbLoader;
import blackboard.data.discussionboard.MessageCounts;

import blackboard.data.course.CourseMembership;
import blackboard.data.user.User;
import blackboard.persist.course.CourseMembershipDbLoader;
import blackboard.persist.user.UserDbLoader;


@SuppressWarnings("serial")
public class MainController extends HttpServlet{

    String cid;
    ConnectionManager cManager = null;
    Connection conn = null;

    public class Usr {
        String username, name, assisted, image, role, email;
        public Usr(String u,  String n, String r, String a, String im, String em){
            this.username = u;
            this.name = n;
            this.role = r;
            this.assisted = a;
            this.image = im;
            this.email = em;
        }
    }

    public class StudentPosts {
        public String forumid, posts;
        public StudentPosts(String f, String p){
            this.forumid = f;
            this.posts = p;
        }  
    }

    public class CourseDefaults {
        public String postslimit, charslimit;
        public CourseDefaults(String p, String c){
            this.postslimit = p;
            this.charslimit = c;
        }
        public CourseDefaults(){}
    }

    private class ForumData {
        String name, id, postslimit, charslimit, initialdate, enddate;
        Boolean isGradingAllowed;
        public ForumData(String i, String n, String p, String c, String id, String ed, Boolean iga){
            this.id = i;
            this.name = n;
            this.postslimit = p;
            this.charslimit = c;
            this.initialdate = id;
            this.enddate = ed;
            this.isGradingAllowed = iga;
        }
    }

    public MainController() {
        super();
        // this.deleteTables("DROP TABLE IF EXISTS prot_forums");
        // this.deleteTables("DROP TABLE IF EXISTS prot_forums_default");
    }

    public String test(String foroid) {
        Id id = ContextManagerFactory.getInstance().getContext().getCourseId();
        Id userid = ContextManagerFactory.getInstance().getContext().getUserId();
        String useridstr = ContextManagerFactory.getInstance().getContext().getUser().getUserName();
        List<User> listas = null;
        CourseMembership cm = null;

        List<Usr> rez = new ArrayList<Usr>();
        String img = null;
        try {
            listas = UserDbLoader.Default.getInstance().loadByCourseId(id);
        } catch (PersistenceException e) {
            e.printStackTrace();
        }
        for (User u : listas){
            try {
                img = blackboard.platform.user.MyPlacesUtil.getAvatarImage(u);
            } catch (Exception e) {e.printStackTrace();}

            try {
                cm = CourseMembershipDbLoader.Default.getInstance().loadByCourseAndUserId(id, u.getId());
            } catch (PersistenceException e) {e.printStackTrace();}

            if (cm.getUserId().equals(userid) && cm.getRoleAsString().equals("STUDENT")) {
                return getForums();
            }
        }
        return "instructor";
    }

    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException {
        String op = request.getParameter("op");
        cid = ContextManagerFactory.getInstance().getContext().getCourse().getCourseId();
        String forumid, postslimit, charslimit, foroid;
        if (op.equals("saveDefaults")) {
            postslimit = request.getParameter("postslimit");
            charslimit = request.getParameter("charslimit");
            response.getWriter().write(this.saveDefaults(postslimit, charslimit));    
        }
        else if (op.equals("getStudentLimits")) {
            foroid = request.getParameter("foroid");
            response.getWriter().write(this.test(foroid));   
        }
        else if (op.equals("studentPosts")) {
            response.getWriter().write(this.getStudentMsgInForum());    
        }
        else if (op.equals("getForums")) {
            response.getWriter().write(this.getForums());    
        }
        else if (op.equals("saveForumDefault")) {
            forumid = request.getParameter("forumid");
            postslimit = request.getParameter("postslimit");
            charslimit = request.getParameter("charslimit");
            response.getWriter().write(this.updateForumDefaults(forumid, postslimit, charslimit));
        }
        else if (op.equals("get")) {
            response.getWriter().write(this.getDefaultsJson());
        }
    }

    public String getDefaultsJson() {
        return new Gson().toJson(this.getDefaults());
    }

    public String deleteTables(String sqlquery) {
        String saveResult = "good" ;
        StringBuffer queryString = new StringBuffer("");
        String s = "good all";
        PreparedStatement selectQuery = null;

        try {
            cManager = BbDatabase.getDefaultInstance().getConnectionManager();
            conn = cManager.getConnection();

            queryString.append(sqlquery);
            selectQuery = conn.prepareStatement(queryString.toString(), ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            ResultSet rSet = selectQuery.executeQuery();

            rSet.close();
            selectQuery.close();

        } catch (java.sql.SQLException sE){
        	
            s += "bad sqlException " + sE.toString() ;
        	
        } catch (ConnectionNotAvailableException cE){
        	
            s += "bad connection not available" ;
           
        } finally {
            if(conn != null){
                cManager.releaseConnection(conn);
            }
        }
        return s;
    }

    public List<CourseDefaults> getForumDefaults(String forumid) {
        String saveResult = "good" ;
        StringBuffer queryString = new StringBuffer("");

        String s = "";
        List<CourseDefaults> rez = new ArrayList<CourseDefaults>();
        PreparedStatement selectQuery = null;

        try {
            cManager = BbDatabase.getDefaultInstance().getConnectionManager();
            conn = cManager.getConnection();

            queryString.append("SELECT postslimit, charslimit ");
            queryString.append("FROM ");
            queryString.append("prot_forums ");
            queryString.append("WHERE foroid=? ");
            queryString.append("AND courseid=? ");

            selectQuery = conn.prepareStatement(queryString.toString(), ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            selectQuery.setString(1, forumid);
            selectQuery.setString(2, this.cid);
            ResultSet rSet = selectQuery.executeQuery();

            while(rSet.next()){
                rez.add(new CourseDefaults(rSet.getString(1), rSet.getString(2)));
            }
            if (rez.isEmpty()) {
                rez = this.getDefaults();
                this.insertForumDefaults(forumid, rez.get(0).postslimit, rez.get(0).charslimit);
            }

            rSet.close();
            selectQuery.close();

        } catch (java.sql.SQLException sE){
        	
            s += "bad sqlException " + sE.toString() ;
        	
        } catch (ConnectionNotAvailableException cE){
        	
            s += "bad connection not available" ;
           
        } finally {
            if(conn != null){
                cManager.releaseConnection(conn);
            }
        }
        return rez;
    }

    
    public String getForums() {
        List<Forum> forums = new LinkedList<Forum>();
        Id id = ContextManagerFactory.getInstance().getContext().getCourseId();
        String s = "";
        Gson gson = new Gson();
        try {
            ForumDbLoader forumDbLoader = ForumDbLoader.Default.getInstance();
            ConferenceDbLoader conferenceDbLoader = ConferenceDbLoader.Default.getInstance();
            List<Conference> conferences = conferenceDbLoader.loadAllByCourseId(id);
            List<ForumData> rez = new ArrayList();
            for (Conference conference : conferences) {
                List<Forum> aux = forumDbLoader.loadByConferenceId(conference.getId());
                for (Forum forum : aux) {
                    List<CourseDefaults> lst = getForumDefaults(forum.getId().toExternalString());
                    if (lst.size() != 0) {
                        String initialdate = forum.getStartDate() != null ? forum.getStartDate().getTime().toString() : "";
                        String enddate = forum.getEndDate() != null ? forum.getEndDate().getTime().toString() : "";
                        rez.add(new ForumData(forum.getId().toExternalString(), 
                                              forum.getTitle(),
                                              lst.get(0).postslimit, 
                                              lst.get(0).charslimit,
                                              initialdate,
                                              enddate,
                                              forum.getProperties().getAllowForumGrading()));
                    }
                    
                    forums.add(forum);
                }
            }
            s = gson.toJson(rez);

        } catch(PersistenceException ex) {
            System.out.println("persistence exception error");
        }
        return s;
    }

    public String getStudentMsgInForum() {

        Id id = ContextManagerFactory.getInstance().getContext().getCourseId();
        Id userid = ContextManagerFactory.getInstance().getContext().getUserId();
        String s = "";
        Gson gson = new Gson();
        int ats = 0;
        List<StudentPosts> rez = new ArrayList();


        try {
            ForumDbLoader forumDbLoader = ForumDbLoader.Default.getInstance();
            ConferenceDbLoader conferenceDbLoader = ConferenceDbLoader.Default.getInstance();
            List<Conference> conferences = conferenceDbLoader.loadAllByCourseId(id);
            MessageDbLoader messageDbLoader = MessageDbLoader.Default.getInstance();
            for (Conference conference : conferences) {
                List<Forum> forumlist = forumDbLoader.loadByConferenceId(conference.getId());
                for (Forum forum : forumlist) {
                    MessageCounts mc = messageDbLoader.loadMessageCountsByForumId(forum.getId(), userid);
                    int totalPostsForum = mc.getTotalCount();
                    rez.add(new StudentPosts(forum.getId().toExternalString(), "" + totalPostsForum));
                }
            }}
        catch(PersistenceException ex) {
            return "algo ha pasado error ";
        }
        return gson.toJson(rez);
    }

    public List<CourseDefaults> getDefaults() {
        String saveResult = "good" ;
        StringBuffer queryString = new StringBuffer("");

        String s = "";
        List<CourseDefaults> rez = new ArrayList<CourseDefaults>();
        PreparedStatement selectQuery = null;

        try {
            cManager = BbDatabase.getDefaultInstance().getConnectionManager();
            conn = cManager.getConnection();

            queryString.append("SELECT postslimit, charslimit ");
            queryString.append("FROM ");
            queryString.append("prot_forums_default ");
            queryString.append("WHERE courseid=? ");

            selectQuery = conn.prepareStatement(queryString.toString(), ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            selectQuery.setString(1, this.cid);
            ResultSet rSet = selectQuery.executeQuery();

            while(rSet.next()){
                rez.add(new CourseDefaults(rSet.getString(1), rSet.getString(2)));
            }
            if (rez.isEmpty()) {
                rez.add(new CourseDefaults("0", "0"));
            }
            s = new Gson().toJson(rez);
            rSet.close();
            selectQuery.close();

        } catch (java.sql.SQLException sE){
        	
            s += "bad sqlException " + sE.toString() ;
        	
        } catch (ConnectionNotAvailableException cE){
        	
            s += "bad connection not available" ;
           
        } finally {
            if(conn != null){
                cManager.releaseConnection(conn);
            }
        }
        return rez;
    }

    public boolean checkIfAlreadyExists(String pkey) {
        StringBuffer queryString = new StringBuffer("");
        PreparedStatement selectQuery = null;
        String cid = ContextManagerFactory.getInstance().getContext().getCourse().getCourseId();

        try {
            cManager = BbDatabase.getDefaultInstance().getConnectionManager();
            conn = cManager.getConnection();
            queryString.append("SELECT 1 ");
            queryString.append("FROM ");
            queryString.append("prot_forums_default ");
            queryString.append("WHERE courseid=? ");

            selectQuery = conn.prepareStatement(queryString.toString(), ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            selectQuery.setString(1, cid);
            ResultSet rSet = selectQuery.executeQuery();
            if(rSet.next()){
                return true;
            }
            return false;
        }
        catch (java.sql.SQLException sE){}
        catch (ConnectionNotAvailableException cE){}
        finally {
            if(conn != null){
                cManager.releaseConnection(conn);
            }
        }
        return true;
    }

    public String insertForumDefaults(String forumid, String postslimit, String charslimit) {
        String saveResult = "updated" ;
        StringBuffer queryString = new StringBuffer("");

        try {
            cManager = BbDatabase.getDefaultInstance().getConnectionManager();
            conn = cManager.getConnection();

            queryString.append("INSERT INTO prot_forums ");
            queryString.append("(foroid, courseid, postslimit, charslimit)");
            queryString.append("VALUES (?,?,?,?)");

            PreparedStatement insertQuery = conn.prepareStatement(queryString.toString());
            insertQuery.setString(1, forumid);
            insertQuery.setString(2, this.cid);
            insertQuery.setString(3, postslimit);
            insertQuery.setString(4, charslimit);

            int insertResult = insertQuery.executeUpdate();
            
            if(insertResult != 1){
                saveResult = "bad while inserting " ;
            	
            }
            insertQuery.close();

        } catch (java.sql.SQLException sE){
        	
            saveResult = "bad sqlException " + sE.toString() ;
        	
        } catch (ConnectionNotAvailableException cE){
        	
            saveResult = "bad connection not available" ;
           
        } finally {
            if(conn != null){
                cManager.releaseConnection(conn);
            }
        }
        return saveResult ;
		
    }

    public String updateForumDefaults(String forumid, String postslimit, String charslimit) {
        String saveResult = "updated" ;
        StringBuffer queryString = new StringBuffer("");

        try {
            cManager = BbDatabase.getDefaultInstance().getConnectionManager();
            conn = cManager.getConnection();

            queryString.append("UPDATE prot_forums ");
            queryString.append("SET postslimit=?, charslimit=? ");
            queryString.append("WHERE foroid=? ");
            queryString.append("AND courseid=? ");


            PreparedStatement insertQuery = conn.prepareStatement(queryString.toString());

            insertQuery.setString(1, postslimit);
            insertQuery.setString(2, charslimit);
            insertQuery.setString(3, forumid);
            insertQuery.setString(4, this.cid);

            int insertResult = insertQuery.executeUpdate();
            
            if(insertResult != 1){
                saveResult = "bad while inserting " ;
            	
            }
            insertQuery.close();

        } catch (java.sql.SQLException sE){
        	
            saveResult = "bad sqlException " + sE.toString() ;
        	
        } catch (ConnectionNotAvailableException cE){
        	
            saveResult = "bad connection not available" ;
           
        } finally {
            if(conn != null){
                cManager.releaseConnection(conn);
            }
        }
        return saveResult ;
		
    }

    public String updateDefaults(String postslimit, String charslimit) {
        String saveResult = "updated" ;
        StringBuffer queryString = new StringBuffer("");
        String cid = ContextManagerFactory.getInstance().getContext().getCourse().getCourseId();

        try {
            cManager = BbDatabase.getDefaultInstance().getConnectionManager();
            conn = cManager.getConnection();

            queryString.append("UPDATE prot_forums_default ");
            queryString.append("SET postslimit=?, charslimit=? ");
            queryString.append("WHERE courseid=? ");

            PreparedStatement insertQuery = conn.prepareStatement(queryString.toString());
            insertQuery.setString(1, postslimit);
            insertQuery.setString(2, charslimit);
            insertQuery.setString(3, cid);

            int insertResult = insertQuery.executeUpdate();
            
            if(insertResult != 1){
                saveResult = "bad while inserting " ;
            	
            }
            insertQuery.close();

        } catch (java.sql.SQLException sE){
        	
            saveResult = "bad sqlException " + sE.toString() ;
        	
        } catch (ConnectionNotAvailableException cE){
        	
            saveResult = "bad connection not available" ;
           
        } finally {
            if(conn != null){
                cManager.releaseConnection(conn);
            }
        }
		
        return saveResult ;
		
    }

    public String saveDefaults(String postslimit, String charslimit) {
        String saveResult = "save first time" ;
        StringBuffer queryString = new StringBuffer("");
        String cid = ContextManagerFactory.getInstance().getContext().getCourse().getCourseId();
        if (checkIfAlreadyExists(cid)) {
            return updateDefaults(postslimit, charslimit);
        }
        try {
            cManager = BbDatabase.getDefaultInstance().getConnectionManager();
            conn = cManager.getConnection();

            queryString.append("INSERT INTO prot_forums_default");
            queryString.append("(courseid, postslimit, charslimit)");
            queryString.append("VALUES (?,?,?)");

            PreparedStatement insertQuery = conn.prepareStatement(queryString.toString());
            insertQuery.setString(1, cid);
            insertQuery.setString(2, postslimit);
            insertQuery.setString(3, charslimit);
            int insertResult = insertQuery.executeUpdate();
            
            if(insertResult != 1){
            	
                saveResult = "bad while inserting " ;
            	
            }
            insertQuery.close();

        } catch (java.sql.SQLException sE){
        	
            saveResult = "bad sqlException " + sE.toString() ;
        	
        } catch (ConnectionNotAvailableException cE){
        	
            saveResult = "bad connection not available" ;
           
        } finally {
            if(conn != null){
                cManager.releaseConnection(conn);
            }
        }
        return saveResult ;
    }
}

package com.controller;

import java.io.IOException;

import java.net.InetAddress;
import java.net.UnknownHostException;
import blackboard.servlet.renderinghook.RenderingHook;

import java.util.Date;

public class TopFrameHook implements RenderingHook {
    
    public String getKey() {
        // return "jsp.topFrame.start";
        return "tag.learningSystemPage.start";
    }

    public String getContent() {
        String s = "https://demo.prot-os.com/webapps/prot-Building%20block-BBLEARN/jshack.js";
        // String s = "http://localhost:9876/webapps/prot-Building%20block-BBLEARN/jshack.js";
        try {
            InetAddress ip = InetAddress.getLocalHost(); // TODO: extract url somehow
            String hostname = ip.getHostName();
            System.out.println("This is the host name: " + hostname);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        return "<script type=\"text/javascript\" src=\"" + s + "\"> </script>";
    }
}


